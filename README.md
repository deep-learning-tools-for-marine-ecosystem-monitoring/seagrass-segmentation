# Seagrass segmentation using U-Net

These are a series of notebooks for semantic segmentation. In semantic segmentation the goal is to predict for each pixel in an image a label. To solve it we use U-net, which is a fully convolutional neural network and train it in a supervised setting (meaning we learn from examples). Our application is on finding seagrass on drone images but our code and the network used can be applied to other problems in even other domains. 

# Installation

**Get the code and data**

<code>git clone https://git.wur.nl/deep-learning-tools-for-marine-ecosystem-monitoring/seagrass-segmentation.git</code>

**Anaconda**

Download and install from: https://www.anaconda.com/

**PyTorch**

Get the install command from: https://pytorch.org/. You have to select if you want to use GPU or CPU only. Afterwards open **Anaconda prompt** if you are on Windows or a terminal if you are on Linux and run the command.

For example the command to run to install PyTorch with CPU only is the following (tested 2022-04-13)

<code>conda install pytorch torchvision cpuonly -c pytorch</code>
    
**Additional libraries**

Run the following commands in either **Anaconda prompt** or the terminal.

<code>pip install tqdm</code>

<code>pip install wget</code>

## Usage

Open **Anaconda prompt** if you are on Windows or a terminal if you are on Linux, go to the directory where you have downloaded the code and run the command.

<code>jupyter lab</code>

You can now run the notebooks from the webbrowser. Make sure the path to the data is set correctly in '0. Config.ipynb'.
